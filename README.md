# Introduction #

This is an Internet Explorer plugin that allows for serializing the DOM in order to collect data needed for Web Consistency Testing.  It was in active use by Mogotest and tested to work in IE 6 - IE 11.
The plugin was originally combined with [SnapsIE](https://github.com/nirvdrum/snapsie), although over time much of SnapsIE's code was folded into Selenium.  The impetus for this plugin was the terribly
slow JavaScript engine in IE 6 & IE 7, combined with the lack of native JSON support.  Serializing the DOM via JavaScript in one of those browsers would routinely take over 20 minutes, whereas with this
plugin the results would typically come back within 1.5 seconds.  The plugin was used with other versions of Internet Explorer for consistent handling across all IE versions and because direct access
to the MSHTML API is substantially faster than JavaScript, even in modern IE versions.

The code is licensed under the Apache License, version 2.0.  This code is being provided as is.  It was developed for internal use by Mogotest, but Mogotest is no longer in business.  The plugin
would be widely applicable to Web Consistency Testing implementations, but the project is in search of a new maintainer.

# Compiling the Mogotest IE Plugin #

There unfortunately is no formal build process.  Visual Studio 2010 was the last actively used IDE for the project and artifacts were built by using the Release build configuration in the included
Visual C++ project.  For historical reasons, the resulting artifact is called SnapsIE (if any enterprising individual would like to change this, it'd be warmly accepted).
